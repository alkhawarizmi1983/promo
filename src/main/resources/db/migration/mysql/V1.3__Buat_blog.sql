create table post(
	id integer primary key auto_increment,
	title varchar(255),
	content text,
	image text,
	created timestamp,
	modified timestamp,
	author varchar(36)
);

insert into post(title, content, image, created, modified, author)
values
('You Cant Build an iPhone With Python', 'A friend of mine recently recounted an interaction with one of her co-workers. Described as a generally nice guy, he had attempted to convince her that code camps were “basically the new engineering degree.” He had gone to one himself, and, in the end, the two of them had ended up in the same place. This, understandably, gave her pause; after all, she had attended the University of Waterloo’s computer bla bla bla', 'https://cdn-images-1.medium.com/max/2000/1*uSin6brD1T_f-lrwQyFdDQ.jpeg', now(), now(), 'caleb'),
('Goodbye, Object Oriented Programming', 'I’ve been programming in Object Oriented languages for decades. The first OO language I used was C++ and then Smalltalk and finally .NET and Java.

I was gung-ho to leverage the benefits of Inheritance, Encapsulation, and Polymorphism. The Three Pillars of the Paradigm.

I was eager to gain the promise of Reuse and leverage the wisdom gained by those who came before me in this new and exciting landscape.

I couldn’t contain my excitement at the thought of mapping my real-world objects into their Classes and expected the whole world to fall neatly into place.', 'https://cdn-images-1.medium.com/max/1600/1*cBFSQ9Ytv_D0jwGtpuL5WA.png', now(), now(), 'esauter'),

('How I went from newbie to Software Engineer in 9 months while working full time', 'Whenever I would start reading a success story, I would immediately look to find the author’s background, hoping it would match mine. I never found someone who had the same background as I did, and most likely mine won’t match yours exactly.

Nonetheless, I hope that my story inspires others and acts as a valuable data point that can be added to your success story dataset.', 'https://cdn-images-1.medium.com/max/1600/1*wyxuq21keffc5b0d_lMkUw.jpeg', now(), now(), 'quette'),

('The State of Web Browsers', 'Yesterday, Windows Central published a rumour that Microsoft is ditching its Edge browser, or more accurately put, to relaunch a new browser using the Chromium engine. The rumour has been picked up by mainstream media and as far as I know, not denied by Microsoft, therefore I assume it to be factual. A good reason as any for me to share some thoughts on the current landscape of web browsers.

If you’re new to my blog, I’ll add the background that I’ve been in the web game since 1996, and have seen every iteration of the browser wars up close. In terms of mindset, I’m from the Zeldman school of thought: a deep believer and proponent of the open web, web standards, a shared web.', 'https://cdn-images-1.medium.com/max/1600/1*ysSQaD2xD85QT2Zz2kFxZw.png', now(), now(), 'ahmed');

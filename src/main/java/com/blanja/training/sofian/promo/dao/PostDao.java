package com.blanja.training.sofian.promo.dao;

import com.blanja.training.sofian.promo.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PostDao extends PagingAndSortingRepository<Post, Integer> {
    Optional<Post> findOneByAuthor(String author);
}

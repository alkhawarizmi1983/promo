package com.blanja.training.sofian.promo.controller;

import com.blanja.training.sofian.promo.dao.PostDao;
import com.blanja.training.sofian.promo.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class BlogController {

    @Autowired
    private PostDao postDao;

    @GetMapping("/posts")
    public Iterable<Post> allAuthors() {
        Iterable<Post> posts = postDao.findAll();

        return posts;
    }

    @GetMapping("/post/{author}")
    public Optional<Post> allAuthors(@PathVariable("author") String author) {
        Optional<Post> post = postDao.findOneByAuthor(author);

        return post;
    }
}

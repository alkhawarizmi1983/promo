package com.blanja.training.sofian.promo.dao;

import com.blanja.training.sofian.promo.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {

}

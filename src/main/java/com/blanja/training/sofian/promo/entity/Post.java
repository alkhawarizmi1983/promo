package com.blanja.training.sofian.promo.entity;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;


/**
 create table post(
 id integer primary key auto_increment,
 title varchar(255),
 content text,
 image text,
 created timestamp,
 modified timestamp,
 author varchar(36),
 FOREIGN KEY (author) REFERENCES author(id)
 );
 * */
@Entity
@Table(name = "post")
@Data
public class Post {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty
    @Size(min = 3, max = 255)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String content;

    @Column(columnDefinition = "TEXT")
    private String image;

    @Basic(optional = false)
    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Basic(optional = false)
    @Column(name = "modified", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    private String author;
}

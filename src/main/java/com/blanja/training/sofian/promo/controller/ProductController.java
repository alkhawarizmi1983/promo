package com.blanja.training.sofian.promo.controller;

import com.blanja.training.sofian.promo.dao.ProductDao;
import com.blanja.training.sofian.promo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
    @Autowired
    private ProductDao productDao;

    @GetMapping("/product/list")
    public Iterable<Product> semuaProduk() {
        return productDao.findAll();
    }

}
